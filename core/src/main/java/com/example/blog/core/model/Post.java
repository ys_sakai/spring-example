package com.example.blog.core.model;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import com.example.blog.core.annotation.CreatedDate;
import com.example.blog.core.annotation.Id;
import com.example.blog.core.annotation.LastModifiedDate;

import lombok.Data;

@Data
public class Post {

    @Id
    private Integer postId;

    @NotNull
    private Integer categoryId;

    @NotNull
    private Integer userId;

    @NotNull
    private String slug;

    @NotNull
    private String title;

    @NotNull
    private String content;

    @CreatedDate
    private LocalDateTime createdAt;

    @LastModifiedDate
    private LocalDateTime updatedAt;

    private Category category;

    private User user;
}
