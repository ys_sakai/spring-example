package com.example.blog.core.model;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import com.example.blog.core.annotation.CreatedDate;
import com.example.blog.core.annotation.Id;
import com.example.blog.core.annotation.LastModifiedDate;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Category {

    @Id
    private Integer categoryId;

    @NotNull
    private String slug;

    @NotNull
    private String name;

    @CreatedDate
    private LocalDateTime createdAt;

    @LastModifiedDate
    private LocalDateTime updatedAt;

    public Category(String slug, String name) {
        this.slug = slug;
        this.name = name;
    }
}
