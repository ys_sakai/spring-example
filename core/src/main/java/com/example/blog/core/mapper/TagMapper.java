package com.example.blog.core.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.example.blog.core.model.Tag;

@Mapper
public interface TagMapper extends BaseMapper<Tag, Integer> {

}
