package com.example.blog.core.repository;

import org.springframework.stereotype.Repository;

import com.example.blog.core.mapper.CategoryMapper;
import com.example.blog.core.model.Category;

@Repository
public class CategoryRepository extends BaseRepository<Category, CategoryMapper, Integer> {

}
