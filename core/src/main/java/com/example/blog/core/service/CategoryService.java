package com.example.blog.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.blog.core.model.Category;
import com.example.blog.core.repository.CategoryRepository;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    public Category findById(Integer id) {
        return categoryRepository.findById(id);
    }

    public Category create(Category category) {
        return categoryRepository.create(category);
    }

    public Category update(Integer id, Category category) {
        return categoryRepository.update(id, category);
    }

    public void delete(Integer id) {
        categoryRepository.delete(id);
    }
}
