package com.example.blog.core.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.example.blog.core.model.User;

@Mapper
public interface UserMapper extends BaseMapper<User, Integer> {

    public User findByUsername(String username);
}
