package com.example.blog.core.mapper;

import java.util.List;

public interface BaseMapper<T, K> {

    public List<T> findAll();

    public T findById(K id);

    public void create(T model);

    public boolean update(T model);

    public void delete(K id);
}
