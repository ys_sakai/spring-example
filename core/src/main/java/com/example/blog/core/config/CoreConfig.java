package com.example.blog.core.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = "com.example.blog.core")
@MapperScan("com.example.blog.core.mapper")
@PropertySource("classpath:/application.properties")
public class CoreConfig {

}
