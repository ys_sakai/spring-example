package com.example.blog.core.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.example.blog.core.model.Post;

@Mapper
public interface PostMapper extends BaseMapper<Post, Integer> {

}
