package com.example.blog.core.model;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import com.example.blog.core.annotation.CreatedDate;
import com.example.blog.core.annotation.Id;
import com.example.blog.core.annotation.LastModifiedDate;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class User {

    @Id
    private Integer userId;

    @NotNull
    private String name;

    @NotNull
    private String email;

    @JsonIgnore
    private String password;

    @CreatedDate
    private LocalDateTime createdAt;

    @LastModifiedDate
    private LocalDateTime updatedAt;
}
