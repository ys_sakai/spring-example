package com.example.blog.core.security;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.example.blog.core.model.User;

import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;

@Component
public class JwtTokenProvider {

    @Value("${security.jwt.secret:secret}")
    private String secret;

    @Value("${security.jwt.expiration:#{60*60*1000}}")
    private long jwtExpirationInMilliseconds;

    private SecretKey secretKey;

    @PostConstruct
    public void init() {
        secretKey = JwtTokenKeys.secretKey(secret);
    }

    public String generateToken(User user) {
        return Jwts.builder()
                .setSubject(Integer.toString(user.getUserId()))
                .setIssuedAt(new Date())
                .setExpiration(expireDateFromNow())
                .signWith(secretKey)
                .compact();
    }

    private Date expireDateFromNow() {
        Date now = new Date();
        return new Date(now.getTime() + jwtExpirationInMilliseconds);
    }

    public Integer getUserIdFromToken(String token) {
        String subject = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();

        return Integer.parseInt(subject);
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            // TODO: ログ出力
            return false;
        }
    }
}
