package com.example.blog.core.repository;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.blog.core.annotation.CreatedDate;
import com.example.blog.core.annotation.Id;
import com.example.blog.core.annotation.LastModifiedDate;
import com.example.blog.core.mapper.BaseMapper;

public abstract class BaseRepository<T, E extends BaseMapper<T, K>, K> {

    @Autowired
    protected E mapper;

    @Autowired
    private ModelMapper modelMapper;

    public List<T> findAll() {
        return mapper.findAll();
    }

    public T findById(K id) {
        return mapper.findById(id);
    }

    public T create(T model) {
        LocalDateTime date = LocalDateTime.now();
        setModelCreatedDate(model, date);
        setModelLastModifiedDate(model, date);

        mapper.create(model);

        return model;
    }

    public T update(K id, T model) {
        T current = mapper.findById(id);

        modelMapper.map(model, current);
        setModelId(current, id);
        setModelLastModifiedDate(current, LocalDateTime.now());

        mapper.update(current);

        return current;
    }

    public void delete(K id) {
        mapper.delete(id);
    }

    protected void setModelId(T model, K id) {
        setModelFieldByAnnotation(model, id, Id.class);
    }

    protected void setModelCreatedDate(T model, LocalDateTime date) {
        setModelFieldByAnnotation(model, date, CreatedDate.class);
    }

    protected void setModelLastModifiedDate(T model, LocalDateTime date) {
        setModelFieldByAnnotation(model, date, LastModifiedDate.class);
    }

    private void setModelFieldByAnnotation(T model, Object value, Class<? extends Annotation> klass) {
        for (Field field : model.getClass().getDeclaredFields()) {
            if (field.getAnnotation(klass) != null) {
                try {
                    field.setAccessible(true);
                    field.set(model, value);
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
