package com.example.blog.core.repository;

import org.springframework.stereotype.Repository;

import com.example.blog.core.mapper.PostMapper;
import com.example.blog.core.model.Post;

@Repository
public class PostRepository extends BaseRepository<Post, PostMapper, Integer> {

}
