package com.example.blog.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.blog.core.model.Tag;
import com.example.blog.core.repository.TagRepository;

@Service
public class TagService {

    @Autowired
    private TagRepository tagRepository;

    public List<Tag> findAll() {
        return tagRepository.findAll();
    }

    public Tag findById(Integer id) {
        return tagRepository.findById(id);
    }

    public Tag create(Tag tag) {
        return tagRepository.create(tag);
    }

    public Tag update(Integer id, Tag tag) {
        return tagRepository.update(id, tag);
    }

    public void delete(Integer id) {
        tagRepository.delete(id);
    }
}
