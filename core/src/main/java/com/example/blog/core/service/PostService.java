package com.example.blog.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.blog.core.model.Post;
import com.example.blog.core.repository.PostRepository;

@Service
public class PostService {

    @Autowired
    private PostRepository postRepository;

    public List<Post> findAll() {
        return postRepository.findAll();
    }

    public Post findById(Integer id) {
        return postRepository.findById(id);
    }

    public Post create(Post post) {
        return postRepository.create(post);
    }

    public Post update(Integer id, Post post) {
        return postRepository.update(id, post);
    }

    public void delete(Integer id) {
        postRepository.delete(id);
    }
}
