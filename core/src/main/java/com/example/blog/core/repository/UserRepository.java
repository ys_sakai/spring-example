package com.example.blog.core.repository;

import org.springframework.stereotype.Repository;

import com.example.blog.core.mapper.UserMapper;
import com.example.blog.core.model.User;

@Repository
public class UserRepository extends BaseRepository<User, UserMapper, Integer> {

    public User findByUsername(String username) {
        return mapper.findByUsername(username);
    }
}
