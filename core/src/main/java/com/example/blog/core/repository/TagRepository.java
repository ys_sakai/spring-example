package com.example.blog.core.repository;

import org.springframework.stereotype.Repository;

import com.example.blog.core.mapper.TagMapper;
import com.example.blog.core.model.Tag;

@Repository
public class TagRepository extends BaseRepository<Tag, TagMapper, Integer> {

}
