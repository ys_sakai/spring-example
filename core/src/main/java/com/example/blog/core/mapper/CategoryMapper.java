package com.example.blog.core.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.example.blog.core.model.Category;

@Mapper
public interface CategoryMapper extends BaseMapper<Category, Integer> {

}
