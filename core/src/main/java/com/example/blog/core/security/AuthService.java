package com.example.blog.core.security;

import com.example.blog.core.model.User;

public interface AuthService {

    public String authenticate(String username, String password);

    public String authenticateById(Integer userId);

    public User register(String username, String email, String password);

    public User getAuthenticatedUser();
}
