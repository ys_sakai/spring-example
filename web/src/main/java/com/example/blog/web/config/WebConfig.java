package com.example.blog.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = "com.example.blog.web")
@PropertySource("classpath:/web.properties")
public class WebConfig {

}
