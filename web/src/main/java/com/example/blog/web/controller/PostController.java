package com.example.blog.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.blog.core.model.Post;
import com.example.blog.core.service.PostService;

@RestController
@RequestMapping("${api.base.path}/posts")
public class PostController {

    @Autowired
    private PostService postService;

    @GetMapping
    public List<Post> findPosts() {
        return postService.findAll();
    }

    @GetMapping("/{id}")
    public Post findPost(@PathVariable Integer id) {
        return postService.findById(id);
    }

    @PostMapping
    public Post createPost(@Validated @RequestBody Post post) {
        return postService.create(post);
    }

    @PutMapping("/{id}")
    public Post updatePost(@PathVariable Integer id, @Validated @RequestBody Post post) {
        return postService.update(id, post);
    }

    @DeleteMapping("/{id}")
    public void deletePost(@PathVariable Integer id) {
        postService.delete(id);
    }
}
