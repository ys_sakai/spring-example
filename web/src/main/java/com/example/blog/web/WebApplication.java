package com.example.blog.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.blog.core.config.CoreConfig;
import com.example.blog.web.config.WebConfig;

@SpringBootApplication(scanBasePackageClasses = { WebConfig.class, CoreConfig.class })
public class WebApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }
}
