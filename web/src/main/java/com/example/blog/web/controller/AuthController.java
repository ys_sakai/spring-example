package com.example.blog.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.blog.core.model.User;
import com.example.blog.core.security.AuthService;
import com.example.blog.web.payload.LoginRequest;
import com.example.blog.web.payload.RegisterRequest;
import com.example.blog.web.payload.TokenAndUser;

@RestController
@RequestMapping("${api.base.path}/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping("/login")
    public TokenAndUser login(@Validated @RequestBody LoginRequest loginRequest) {
        String token = authService.authenticate(loginRequest.getUsername(), loginRequest.getPassword());
        User user = authService.getAuthenticatedUser();
        return new TokenAndUser(token, user);
    }

    @PostMapping("/register")
    public TokenAndUser register(@Validated @RequestBody RegisterRequest registerRequest) {
        User user = authService.register(registerRequest.getUsername(), registerRequest.getEmail(),
                registerRequest.getPassword());
        String token = authService.authenticate(registerRequest.getUsername(), registerRequest.getPassword());
        return new TokenAndUser(token, user);
    }
}
