package com.example.blog.web.payload;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class RegisterRequest {

    @NotNull
    private String username;

    @NotNull
    private String email;

    @NotNull
    private String password;
}
