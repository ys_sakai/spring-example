package com.example.blog.web.payload;

import com.example.blog.core.model.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TokenAndUser {

    private String token;
    private User user;
}
