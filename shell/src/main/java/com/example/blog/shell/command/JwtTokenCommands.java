package com.example.blog.shell.command;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import com.example.blog.core.security.JwtTokenKeys;

@ShellComponent
public class JwtTokenCommands {

    @ShellMethod("JWT Tokenの秘密鍵を生成する")
    public String generateJwtSecret() {
        return JwtTokenKeys.generate();
    }
}
