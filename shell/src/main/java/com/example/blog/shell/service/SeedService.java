package com.example.blog.shell.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.blog.core.model.Category;
import com.example.blog.core.model.Post;
import com.example.blog.core.model.User;
import com.example.blog.core.security.AuthService;
import com.example.blog.core.service.CategoryService;
import com.example.blog.core.service.PostService;

@Service
public class SeedService {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private PostService postService;

    @Autowired
    private AuthService authService;

    public void master() {
        createCategories();
    }

    public void test() {
        createPost();
    }

    private List<Category> createCategories() {
        List<Category> categories = Arrays.asList(
                new Category("category1", "カテゴリー１"),
                new Category("category2", "カテゴリー２"),
                new Category("category3", "カテゴリー３"));

        return categories.stream()
                .map(category -> categoryService.create(category))
                .collect(Collectors.toList());
    }

    private Post createPost() {
//        authService.authenticateById(1);
        User user = authService.getAuthenticatedUser();

        Post post = new Post();
        post.setCategoryId(1);
        post.setUserId(authService.getAuthenticatedUser().getUserId());
        post.setSlug("test");
        post.setTitle("テスト投稿");
        post.setContent("テスト投稿です");

        return postService.create(post);
    }
}
