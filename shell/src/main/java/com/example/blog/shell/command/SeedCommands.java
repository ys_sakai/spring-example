package com.example.blog.shell.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import com.example.blog.core.security.AuthService;
import com.example.blog.shell.service.SeedService;

@ShellComponent
public class SeedCommands {

    @Autowired
    private SeedService seedService;

    @Autowired
    private AuthService authService;

    @ShellMethod("マスタの初期データを作成する")
    public void master() {
        seedService.master();
        System.out.println("master end");
    }

    @ShellMethod("テストデータを作成する")
    public void test() {
        authService.authenticateById(1);
        seedService.test();
    }
}
