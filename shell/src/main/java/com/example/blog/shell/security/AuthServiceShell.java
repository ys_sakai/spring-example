package com.example.blog.shell.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.blog.core.model.User;
import com.example.blog.core.repository.UserRepository;
import com.example.blog.core.security.AuthService;
import com.example.blog.core.security.JwtTokenProvider;

@Service
public class AuthServiceShell implements AuthService {

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserRepository userRepository;

    private User user;

    @Override
    public String authenticate(String username, String password) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new BadCredentialsException("Bad credentials");
        }

        this.user = user;

        return tokenProvider.generateToken(this.user);
    }

    @Override
    public String authenticateById(Integer userId) {
        User user = userRepository.findById(userId);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        this.user = user;

        return tokenProvider.generateToken(this.user);
    }

    @Override
    public User register(String username, String email, String password) {
        User user = new User();
        user.setName(username);
        user.setEmail(email);
        user.setPassword(passwordEncoder.encode(password));
        return userRepository.create(user);
    }

    @Override
    public User getAuthenticatedUser() {
        return user;
    }
}
