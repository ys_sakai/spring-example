package com.example.blog.shell;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.blog.core.config.CoreConfig;
import com.example.blog.shell.config.ShellConfig;

@SpringBootApplication(scanBasePackageClasses = { ShellConfig.class, CoreConfig.class })
public class BatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(BatchApplication.class, args);
    }
}
