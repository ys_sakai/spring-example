package com.example.blog.shell.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = "com.example.blog.shell")
@PropertySource("classpath:/shell.properties")
public class ShellConfig {

}
